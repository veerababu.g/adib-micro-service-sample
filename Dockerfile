FROM oraclejdk8:latest
VOLUME /tmp
ADD customer-service.jar customer-service.jar
EXPOSE
8090
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/customer-service.jar"]