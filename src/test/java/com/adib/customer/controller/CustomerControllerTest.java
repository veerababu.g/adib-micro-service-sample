package com.adib.customer.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.adib.customer.controller.CustomerController;
import com.adib.customer.model.Customer;
import com.adib.customer.service.CustomerService;

@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {
	Customer customer= new Customer();
	Map<String,String> response= new HashMap<>();
	
	@Mock
	CustomerService customerService;
	@InjectMocks
	CustomerController customerController;
	
	@Before
	public void before() {
		customer.setName("G Veerababu");
		customer.setAccountType("Savings");
		response.put("customerId", "1");
		response.put("Status", "success");
		
	}
	@Test
	public void testCreateCustomer() {
		Mockito.when(customerService.createCustomer(customer)).thenReturn(response);
		assertTrue(customerController.createCustomer(customer).getBody().containsValue("success"));
	}
	@Test
	public void testGetCustomerById() {
		Mockito.when(customerService.getCustomerById(1L)).thenReturn(customer);
		assertEquals("G Veerababu", customerController.getCustomerById(1L).getBody().getName());
		
	}
	@Test
	public void testUpdateCustomer() {
		Mockito.when(customerService.updateCustomer(customer)).thenReturn(response);
		assertTrue(customerController.updateCustomer(customer).getBody().containsValue("success"));
	}
	
	@Test
	public void testDeleteCustomerById() {
		Mockito.when(customerService.deleteCustomer(1L)).thenReturn(response);
		assertTrue(customerController.deleteCustomerById(1L).getBody().containsValue("success"));
	}
}
