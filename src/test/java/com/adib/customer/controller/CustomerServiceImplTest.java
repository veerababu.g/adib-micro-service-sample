package com.adib.customer.controller;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.adib.customer.model.Customer;
import com.adib.customer.repository.CustomerRepository;
import com.adib.customer.service.CustomerServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceImplTest {
	Customer customer= new Customer();
	Map<String,String> response= new HashMap<>();
	@Mock
	CustomerRepository customerRepository;
	@InjectMocks
	CustomerServiceImpl customerService;
	@Before
	public void before() {
		customer.setId(1L);
		customer.setName("G Veerababu");
		customer.setAccountType("Savings");
		response.put("customerId", "1");
		response.put("Status", "success");
		
	}
	@Test
	public void testGetCustomerById() {
		Mockito.when(customerRepository.getById(1L)).thenReturn(customer);
		assertEquals("G Veerababu", customerService.getCustomerById(1L).getName());
	}
	@Test
	public void testCreatecustomer() {
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		assertEquals("success",customerService.createCustomer(customer).get("Status"));
		
	}
	
	
	

}
