package com.adib.customer.controller;

import java.util.Map;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adib.customer.model.Customer;
import com.adib.customer.service.CustomerService;

@RestController
@RequestMapping(value = "/customers/v1")
public class CustomerController {
	private static final Logger logger = LogManager.getLogger(CustomerController.class);
	@Autowired
	public CustomerService customerService;

	@GetMapping("/customer/{id}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable("id") Long id) {
		logger.info("Invoked Customer servce controller to get Customer name by id...");
		return new ResponseEntity<Customer>(customerService.getCustomerById(id), HttpStatus.OK);

	}
	@PostMapping("/createCustomer")
	public ResponseEntity<Map<String, String>> createCustomer(@Valid @RequestBody Customer Customer) {
		logger.info("Invoked Customer servce controller to insert Customer Records...");
		return new ResponseEntity<Map<String, String>>(customerService.createCustomer(Customer), HttpStatus.CREATED);
	}
	@PutMapping("/updateCustomer")
	public ResponseEntity<Map<String, String>> updateCustomer(@RequestBody Customer Customer) {
		logger.info("Invoked Customer servce controller to update Customer Records...");
		return new ResponseEntity<Map<String, String>>(customerService.updateCustomer(Customer), HttpStatus.OK);
	}
	@DeleteMapping("/deleteCustomer/{id}")
	public ResponseEntity<Map<String, String>> deleteCustomerById(@PathVariable("id") Long id) {
		logger.info("Invoked Customer servce controller to delete Customer Records...");
		return new ResponseEntity<Map<String, String>>(customerService.deleteCustomer(id), HttpStatus.OK);

	}
}
