package com.adib.customer.service;



import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.adib.customer.exception.CustomerNotFoundException;
import com.adib.customer.model.Customer;
import com.adib.customer.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger logger = LogManager.getLogger(CustomerServiceImpl.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	public CustomerRepository CustomerRepository;
	
	@Override
	public Customer getCustomerById(Long id) {
		logger.info("Invoked Customer servce to get Customer name by id...");
		Customer Customer =CustomerRepository.getById(id);
		if(StringUtils.isEmpty(Customer))
			throw new CustomerNotFoundException("Customer id="+ id+" not found");
		return Customer;
	}

	@Override
	public Map<String, String> createCustomer(Customer Customer) {
		Map<String, String> CustomerResponse= new HashMap<>();
		Customer=CustomerRepository.save(Customer);
		if(!StringUtils.isEmpty(Customer)) {
			CustomerResponse.put("CustomerId", Customer.getId().toString());
			CustomerResponse.put("Status", "success");
		}else {
			CustomerResponse.put("CustomerId", Customer.getId().toString());
			CustomerResponse.put("Status", "failed");
		}
			
	
		return CustomerResponse;
	}

	@Override
	public Map<String, String> updateCustomer(Customer Customer) {
		Map<String, String> CustomerResponse= new HashMap<>();
		Customer=CustomerRepository.saveAndFlush(Customer);
		
		if(!StringUtils.isEmpty(Customer)) {
			CustomerResponse.put("CustomerId", Customer.getId().toString());
			CustomerResponse.put("Status", "success");
		}else {
			CustomerResponse.put("CustomerId", Customer.getId().toString());
			CustomerResponse.put("Status", "failed");
		}
		return CustomerResponse;
	}

	@Override
	public Map<String, String> deleteCustomer(Long id) {
		Map<String, String> CustomerResponse= new HashMap<>();
		CustomerRepository.deleteById(id);;
		CustomerResponse.put("CustomerId", id.toString());
		CustomerResponse.put("Status", "success");
		return CustomerResponse;
	}

}
