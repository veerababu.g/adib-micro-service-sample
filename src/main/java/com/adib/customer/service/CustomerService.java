package com.adib.customer.service;

import java.io.Serializable;
import java.util.Map;

import com.adib.customer.model.Customer;

public interface CustomerService extends Serializable {

	public Customer getCustomerById(Long id);

	public Map<String,String> createCustomer(Customer student);

	public Map<String, String> updateCustomer(Customer student);

	public Map<String, String> deleteCustomer(Long id);
}
