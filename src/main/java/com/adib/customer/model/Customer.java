package com.adib.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(name = "customer")
public class Customer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "customer_id")
	@JsonProperty("CustomerId")
	@JsonInclude(Include.NON_NULL)
	private Long id;
	@JsonProperty("CustomerName")
	@JsonInclude(Include.NON_NULL)
	@NotBlank(message = "Name should not be empty")
	@Column(name = "customer_name")
	@Size(min = 6, message = "Name should have atleast 6 characters")
	private String name;
	@JsonProperty("CustomerAccountType")
	@JsonInclude(Include.NON_NULL)
	@NotBlank(message = "Role should not be empty")
	@Column(name = "account_type")
	private String accountType;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public Customer(Long id,
			@NotBlank(message = "Name should not be empty") @Size(min = 6, message = "Name should have atleast 6 characters") String name,
			@NotBlank(message = "Role should not be empty") String accountType) {
		super();
		this.id = id;
		this.name = name;
		this.accountType = accountType;
	}
	
	public Customer() {}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountType == null) ? 0 : accountType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (accountType == null) {
			if (other.accountType != null)
				return false;
		} else if (!accountType.equals(other.accountType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", accountType=" + accountType + "]";
	}
	
}
