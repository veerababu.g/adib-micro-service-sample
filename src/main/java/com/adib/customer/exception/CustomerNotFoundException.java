package com.adib.customer.exception;

public class CustomerNotFoundException extends RuntimeException{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String error;

	public CustomerNotFoundException() {
		    super();
		  }
	public CustomerNotFoundException(String error){
		super(error);
		this.error = error;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
}
