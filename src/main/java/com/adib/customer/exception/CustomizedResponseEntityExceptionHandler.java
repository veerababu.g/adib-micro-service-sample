package com.adib.customer.exception;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.adib.customer.vo.Error;
import com.adib.customer.vo.Errors;
import com.adib.customer.vo.CustomerError;

@ControllerAdvice
@RestController

public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	private static final Logger logger = LogManager.getLogger(CustomizedResponseEntityExceptionHandler.class);
	 /* @ExceptionHandler(Exception.class)
	  public final ResponseEntity<ErrorDetails> handleAllExceptions(Exception ex, WebRequest request) {
	    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
	        request.getDescription(false));
	    logger.error("Excpetion"+ex.getMessage());
	    return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	  }*/
	  
	 
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		logger.error(CustomizedResponseEntityExceptionHandler.class.getSimpleName() + "_"+ Thread.currentThread().getStackTrace()[1].getMethodName()+"()- Start");
		BindingResult result = ex.getBindingResult();        
		Errors errorObject = processFieldErrors(result.getFieldErrors(), status.toString());			
		
        return new ResponseEntity<>(errorObject, headers, status);
	}
	
	public Errors processFieldErrors(List<org.springframework.validation.FieldError> fieldErrors , String errorCode){
		Errors errorObject=new Errors();
		for(org.springframework.validation.FieldError fieldError : fieldErrors){
			errorObject.add(new Error(errorCode,fieldError.getDefaultMessage(),null,null));
		}
		
		return errorObject;
		
	}	
	
	

	  @ExceptionHandler(CustomerNotFoundException.class)
	  public final ResponseEntity<CustomerError> handleUserNotFoundException(CustomerNotFoundException ex, WebRequest request) {
	    CustomerError errorDetails = new CustomerError(new Date(), ex.getMessage(),
	        request.getDescription(false));
	    logger.error("Excpetion"+ex.getMessage());
	    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	  }

	}
