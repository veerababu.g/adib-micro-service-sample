package com.adib.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adib.customer.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	public Customer getById(Long customerId);
}
